# README #

Implementation of neural networks conditional random fields. The algorithm is a combination of neural networks to extract high level features from the data, and conditional random field to capture the correlation between consecutive letters in an OCR dataset. 

The main class (super-class) is the AbstractLinearChainCRF, and classes LinearChainCRF and NNLinearChainCRF inherit from the super-class. 

I have also implemented a version with context windows.


Requirements:
python 2.7
statsmodels, numpy, scipy and mlpython


To run the code execute:
python run_crf.py