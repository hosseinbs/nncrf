import sys
import math
from statsmodels.graphics.regressionplots import abline_plot

print('Python %s on %s' % (sys.version, sys.platform))
sys.path.extend(['/home/hossein/libs'])

from mlpython.learners.generic import Learner
import numpy as np
from AbstractLinearChainCRF import *

class LinearChainCRF(AbstractLinearChainCRF):
    """
    Linear chain conditional random field. The contex window size
    has a radius of 1.

    Option ``lr`` is the learning rate.

    Option ``dc`` is the decrease constante for the learning rate.

    Option ``L2`` is the L2 regularization weight (weight decay).

    Option ``L1`` is the L1 regularization weight (weight decay).

    Option ``n_epochs`` number of training epochs.

    **Required metadata:**

    * ``'input_size'``: Size of the input.
    * ``'targets'``:    Set of possible targets.

    """

    def __init__(self,
                 lr=0.001,
                 dc=1e-8,
                 L2=0.001,
                 L1=0,
                 n_epochs=10,
                 context_window_size = -2):
        self.lr=lr
        self.dc=dc
        self.L2=L2
        self.L1=L1
        self.n_epochs=n_epochs
        if(context_window_size == 0):
            self.context_window_indices = [0]
        elif(context_window_size < -1):
            self.context_window_indices = np.arange(context_window_size, 1)
        else:
            self.context_window_indices = np.arange(-context_window_size, context_window_size + 1)



        # internal variable keeping track of the number of training iterations since initialization
        self.epoch = 0

    def initialize(self,input_size,n_classes):
        """
        This method allocates memory for the fprop/bprop computations
        and initializes the parameters of the CRF to 0 (DONE)
        """

        self.n_classes = n_classes
        self.input_size = input_size

        # Can't allocate space for the alpha/beta tables of
        # belief propagation (forward-backward), since their size
        # depends on the input sequence size, which will change from
        # one example to another.

        ###########################################
        # Allocate space for the linear chain CRF #
        ###########################################
        # - self.weights[0] are the connections with the image at the current position
        # - self.weights[-1] are the connections with the image on the left of the current position
        # - self.weights[1] are the connections with the image on the right of the current position

        self.weights = np.zeros((len(self.context_window_indices), self.input_size, self.n_classes))

        # - self.bias is the bias vector of the output at the current position
        self.bias = np.zeros((self.n_classes))

        # - self.lateral_weights are the linear chain connections between target at adjacent positions
        self.lateral_weights = np.zeros((self.n_classes,self.n_classes))

        #########################
        # Initialize parameters #
        #########################

        # Since the CRF log factors are linear in the parameters,
        # the optimization is convex and there's no need to use a random
        # initialization.

        self.n_updates = 0 # To keep track of the number of updates, to decrease the learning rate

    def calc_target_unary_log_factors(self, input):

        target_unary_log_factors = np.zeros(shape=(self.n_classes, input.shape[0]))


        for x_k_index, x_k in enumerate(input):
            a_xk = 0
            for weight_index, context_window_index in enumerate(self.context_window_indices):
                if (x_k_index + context_window_index) >= 0 and (x_k_index + context_window_index) < input.shape[0]:

                    a_xk += np.dot(input[x_k_index + context_window_index], self.weights[weight_index,:,:])

            a_xk += self.bias

            target_unary_log_factors[:, x_k_index] = a_xk

        return target_unary_log_factors

    def bprop(self, input, target, log_marginal_probs, log_pairwise_marginal_probs):

        """
        Backpropagation:
        - fills in the CRF gradients of the weights, lateral weights and bias
          in self.grad_weights, self.grad_lateral_weights and self.self.grad_bias
        - returns nothing
        Argument ``input`` is a Numpy 2D array where the number of
        rows if the sequence size and the number of columns is the
        input size.
        Argument ``target`` is a Numpy 1D array of integers between
        0 and nb. of classe - 1. Its size is the same as the number of
        rows of argument ``input``.
        """
        self.grad_weights = np.zeros(shape=self.weights.shape)
        self.grad_bias = np.zeros(shape=self.bias.shape)
        self.grad_lateral_weights = np.zeros(shape=self.lateral_weights.shape)

        for k in range(len(target)):

            e_k = np.zeros((self.n_classes))
            e_kplus1 = np.zeros((self.n_classes))

            e_k[target[k]] = 1
            grad_bias_update = -1 * (e_k - np.exp(log_marginal_probs[:,k]))

            for weight_index, context_window_index in enumerate(self.context_window_indices):
                if (k + context_window_index) >= 0 and (k + context_window_index) < input.shape[0]:
                    self.grad_weights[weight_index,:,:] += np.dot(np.matrix(input[k + context_window_index, :]).T, np.matrix(grad_bias_update))

            self.grad_bias += grad_bias_update

            if k < (len(target) - 1):
                e_kplus1[target[k+1]] = 1
                self.grad_lateral_weights += -1 * (np.dot(np.matrix(e_k).T, np.matrix(e_kplus1)) - np.exp(log_pairwise_marginal_probs[:,:,k]))

        self.grad_weights = self.grad_weights + 2 * self.L2 * np.array(self.weights)


    def update(self):
        """
        Stochastic gradient update:
        - performs a gradient step update of the CRF parameters self.weights,
          self.lateral_weights and self.bias, using the gradients in
          self.grad_weights, self.grad_lateral_weights and self.grad_bias
        """
        self.weights = self.weights - self.lr * self.grad_weights
        self.lateral_weights = self.lateral_weights - self.lr * self.grad_lateral_weights
        self.bias = self.bias - self.lr * self.grad_bias

    def verify_gradients(self):
        """
        Verifies the implementation of the fprop and bprop methods
        using a comparison with a finite difference approximation of
        the gradients.
        """
        print 'WARNING: calling verify_gradients reinitializes the learner'

        rng = np.random.mtrand.RandomState(1234)

        self.initialize(10,3)
        example = (rng.rand(4,10),np.array([0,1,1,2]))
        input,target = example
        epsilon=1e-6
        self.lr = 0.1
        self.decrease_constant = 0

        for weight_ind in range(len(self.context_window_indices)):
            self.weights[weight_ind, :, :] += 0.01*rng.rand(self.input_size,self.n_classes)

        self.bias = 0.01*rng.rand(self.n_classes)
        self.lateral_weights = 0.01*rng.rand(self.n_classes,self.n_classes)

        loss, log_marginal_probs, log_pairwise_marginal_probs,p_y_given_x = self.fprop(input, target)
        self.bprop(input,target, log_marginal_probs, log_pairwise_marginal_probs)

        import copy
        emp_grad_weights = copy.deepcopy(self.weights)

        for h in range(len(self.weights)):
            for i in range(self.weights[h].shape[0]):
                for j in range(self.weights[h].shape[1]):
                    self.weights[h][i,j] += epsilon

                    loss_a, log_marginal_probs_a, log_pairwise_marginal_probs_a, p_y_given_x_a = self.fprop(input, target)
                    self.weights[h][i,j] -= epsilon

                    self.weights[h][i,j] -= epsilon
                    loss_b, log_marginal_probs_b, log_pairwise_marginal_probs_b, p_y_given_x_b = self.fprop(input, target)
                    self.weights[h][i,j] += epsilon

                    emp_grad_weights[h][i,j] = (loss_a-loss_b)/(2.*epsilon)

        for weight_ind, context_window_index  in enumerate(self.context_window_indices):
            print('grad_weights['+ str(context_window_index) +'] diff.:' +  str(np.sum(np.abs(self.grad_weights[weight_ind,:,:].ravel()-emp_grad_weights[weight_ind,:,:].ravel()))/self.weights[weight_ind,:,:].ravel().shape[0]))

        emp_grad_lateral_weights = copy.deepcopy(self.lateral_weights)

        for i in range(self.lateral_weights.shape[0]):
            for j in range(self.lateral_weights.shape[1]):
                self.lateral_weights[i,j] += epsilon
                loss_a, log_marginal_probs_a, log_pairwise_marginal_probs_a, p_y_given_x_a = self.fprop(input, target)
                self.lateral_weights[i,j] -= epsilon

                self.lateral_weights[i,j] -= epsilon
                loss_b, log_marginal_probs_b, log_pairwise_marginal_probs_b, p_y_given_x_b = self.fprop(input, target)
                self.lateral_weights[i,j] += epsilon

                emp_grad_lateral_weights[i,j] = (loss_a-loss_b)/(2.*epsilon)


        print 'grad_lateral_weights diff.:',np.sum(np.abs(self.grad_lateral_weights.ravel()-emp_grad_lateral_weights.ravel()))/self.lateral_weights.ravel().shape[0]

        emp_grad_bias = copy.deepcopy(self.bias)
        for i in range(self.bias.shape[0]):
            self.bias[i] += epsilon
            loss_a, log_marginal_probs_a, log_pairwise_marginal_probs_a, p_y_given_x_a = self.fprop(input, target)
            self.bias[i] -= epsilon

            self.bias[i] -= epsilon
            loss_b, log_marginal_probs_b, log_pairwise_marginal_probs_b, p_y_given_x_b = self.fprop(input, target)
            self.bias[i] += epsilon

            emp_grad_bias[i] = (loss_a-loss_b)/(2.*epsilon)

        print 'grad_bias diff.:',np.sum(np.abs(self.grad_bias.ravel()-emp_grad_bias.ravel()))/self.bias.ravel().shape[0]
