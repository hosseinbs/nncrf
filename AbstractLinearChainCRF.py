import sys
import math
from statsmodels.graphics.regressionplots import abline_plot

print('Python %s on %s' % (sys.version, sys.platform))
sys.path.extend(['/home/hossein/libs'])

from mlpython.learners.generic import Learner
import numpy as np
from abc import ABCMeta, abstractmethod


class AbstractLinearChainCRF(object, Learner):
    """
    Linear chain conditional random field. The contex window size
    has a radius of 1.

    Option ``lr`` is the learning rate.

    Option ``dc`` is the decrease constante for the learning rate.

    Option ``L2`` is the L2 regularization weight (weight decay).

    Option ``L1`` is the L1 regularization weight (weight decay).

    Option ``n_epochs`` number of training epochs.

    **Required metadata:**

    * ``'input_size'``: Size of the input.
    * ``'targets'``:    Set of possible targets.

    """
    @abstractmethod
    def initialize(self, crf_input_size, n_classes):
        """ This method should be implemented for each subclass separately """
        pass

    def forget(self):
        """
        Resets the neural network to its original state (DONE)
        """
        self.initialize(self.input_size,self.n_classes)
        self.epoch = 0

    def train(self, trainset):
        """
        Trains the neural network until it reaches a total number of
        training epochs of ``self.n_epochs`` since it was
        initialize. (DONE)

        Field ``self.epoch`` keeps track of the number of training
        epochs since initialization, so training continues until
        ``self.epoch == self.n_epochs``.

        If ``self.epoch == 0``, first initialize the model.
        """
        self.lr = self.lr - self.dc

        if self.epoch == 0:
            input_size = trainset.metadata['input_size']
            n_classes = len(trainset.metadata['targets'])
            self.initialize(input_size, n_classes)

        print self.lr

        for it in range(self.epoch,self.n_epochs):
            for input,target in trainset:

                loss, log_marginal_probs, log_pairwise_marginal_probs,p_y_given_x = self.fprop(input, target)
                self.bprop(input,target, log_marginal_probs, log_pairwise_marginal_probs)
                self.update()

                # loss, log_marginal_probs, log_pairwise_marginal_probs,p_y_given_x = self.fprop(input, target)



        self.epoch = self.n_epochs

    @abstractmethod
    def calc_target_unary_log_factors(self, input):
        pass

    def fprop(self,input,target):
        """
        Forward propagation:
        - computes the value of the unary log factors for the target given the input (the field
          self.target_unary_log_factors should be assigned accordingly)
        - computes the alpha and beta tables using the belief propagation (forward-backward)
          algorithm for linear chain CRF (the field ``self.alpha`` and ``self.beta``
          should be allocated and filled accordingly)
        - returns the training loss, i.e. the
          regularized negative log-likelihood for this (``input``,``target``) pair
        Argument ``input`` is a Numpy 2D array where the number of
        rows if the sequence size and the number of columns is the
        input size.
        Argument ``target`` is a Numpy 1D array of integers between
        0 and nb. of classe - 1. Its size is the same as the number of
        rows of argument ``input``.
        """
        target_unary_log_factors = self.calc_target_unary_log_factors(input)

        log_alpha, log_beta, log_z_x = self.belief_propagation(input, target_unary_log_factors)

        loss = self.training_loss(target,target_unary_log_factors, log_z_x)

        marginal_probs = self.compute_log_marginal_probabilities(log_alpha, log_beta, target_unary_log_factors, input.shape[0])
        assert(np.sum(marginal_probs > 0) == 0)

        pairwise_marginal_probs = self.compute_log_pairwise_marginal_probabilities(log_alpha, log_beta, target_unary_log_factors, input.shape[0])
        assert(np.sum(pairwise_marginal_probs > 0) == 0)

        nominator = 0
        for label_index, label in enumerate(target):
            if label_index < len(target) - 1:
                nominator += target_unary_log_factors[label, label_index] + self.lateral_weights[label, target[label_index+1]]
            else:
                nominator += target_unary_log_factors[label, label_index]

        p_y_given_x = math.exp(nominator - log_z_x)

        return loss, marginal_probs, pairwise_marginal_probs, p_y_given_x

    def compute_log_marginal_probabilities(self, log_alpha, log_beta, target_unary_log_factors, K):

        # TODO: do not forget to set precision of numpy arrays (P(y)'s)
        p_y_k_given_X = np.zeros(shape=(self.n_classes, K))

        for y_k in range(self.n_classes):
            p_y_k_given_X[y_k, 0] = target_unary_log_factors[y_k, 0] + log_beta[y_k, 0]

        for k in range(1, K - 1):
            for y_k in range(self.n_classes):
                p_y_k_given_X[y_k, k] = target_unary_log_factors[y_k, k] + log_alpha[y_k, k-1] + log_beta[y_k, k]

        for y_k in range(self.n_classes):
            p_y_k_given_X[y_k, -1] = target_unary_log_factors[y_k, -1] + log_alpha[y_k, -1]

        return p_y_k_given_X - np.log(np.sum(np.exp(p_y_k_given_X), axis = 0))

    def compute_log_pairwise_marginal_probabilities(self, log_alpha, log_beta, target_unary_log_factors, K):

        p_y_k_y_kplusone_given_X = np.zeros(shape=(self.n_classes, self.n_classes, K - 1))

        for y_k in range(self.n_classes):
            for y_kplusone in range(self.n_classes):
                p_y_k_y_kplusone_given_X [y_k, y_kplusone, 0] = target_unary_log_factors[y_k, 0] \
                                                                + self.lateral_weights[y_k, y_kplusone] + log_alpha[y_k, 0] \
                                                                + target_unary_log_factors[y_kplusone, 1] + log_beta[y_kplusone, 1]

        for k in range(1, K - 1):
            for y_k in range(self.n_classes):
                for y_kplusone in range(self.n_classes):
                    if k == K - 2:
                        p_y_k_y_kplusone_given_X [y_k, y_kplusone, k] = target_unary_log_factors[y_k, k] + self.lateral_weights[y_k, y_kplusone] + \
                                                                        target_unary_log_factors[y_kplusone, k+1] + log_alpha[y_k, k]
                    else:
                        p_y_k_y_kplusone_given_X [y_k, y_kplusone, k] = target_unary_log_factors[y_k, k] + self.lateral_weights[y_k, y_kplusone] + \
                                                                        target_unary_log_factors[y_kplusone, k+1] + \
                                                                        log_alpha[y_k, k] + log_beta[y_kplusone, k+1]

        for k in range(K-1):
            z_k = np.sum(np.exp(p_y_k_y_kplusone_given_X[:,:,k]))
            p_y_k_y_kplusone_given_X [:, :, k] = p_y_k_y_kplusone_given_X [:, :, k] - math.log(z_k)

        return p_y_k_y_kplusone_given_X

    def calc_log_alpha_table(self, target_unary_log_factors, input):

        #compute Alpha table
        # alpha = np.zeros(shape = (self.n_classes, input.shape[0]-1))
        log_alpha = np.zeros(shape = (self.n_classes, input.shape[0]-1))
        for y_prime_2 in range(self.n_classes):
            z_i_array = np.zeros(shape = (self.n_classes))
            for y_prime_1 in range(self.n_classes):
                # alpha[y_prime_2, 0] += math.exp(target_unary_log_factors[y_prime_1 , 0] + self.lateral_weights[y_prime_1, y_prime_2])
                z_i_array[y_prime_1] = target_unary_log_factors[y_prime_1 , 0] + self.lateral_weights[y_prime_1, y_prime_2]
            max_z_i = np.max(z_i_array)
            log_alpha[y_prime_2, 0] = math.log(np.sum(np.exp(z_i_array - max_z_i))) + max_z_i

        for k in range(1, input.shape[0]-1):
            for y_prime_k_plus1 in range(self.n_classes):
                z_i_array = np.zeros(shape = (self.n_classes))
                for y_prime_k in range(self.n_classes):
                    # alpha[y_prime_k_plus1, k] += math.exp(target_unary_log_factors[y_prime_k, k] + self.lateral_weights[y_prime_k, y_prime_k_plus1])\
                    #                                   * alpha[y_prime_k,k - 1]
                    z_i_array[y_prime_k] = target_unary_log_factors[y_prime_k, k] + self.lateral_weights[y_prime_k, y_prime_k_plus1] \
                                           + log_alpha[y_prime_k,k - 1]
                max_z_i = np.max(z_i_array)
                log_alpha[y_prime_k_plus1, k] = math.log(np.sum(np.exp(z_i_array - max_z_i))) + max_z_i

        # Z_of_X = 0
        z_i_array = np.zeros(shape = (self.n_classes))
        for y_prime_K in range(self.n_classes):
            # Z_of_X += math.exp(target_unary_log_factors[y_prime_K, -1]) * alpha[y_prime_K, -1]
            z_i_array[y_prime_K] = target_unary_log_factors[y_prime_K, -1] + log_alpha[y_prime_K, -1]

        max_z_i = np.max(z_i_array)
        log_Z_of_X = math.log(np.sum(np.exp(z_i_array - max_z_i))) + max_z_i

        # print math.log(Z_of_X)
        # print log_Z_of_X

        return log_alpha, log_Z_of_X

    def calc_log_beta_table(self, target_unary_log_factors, input):

        # beta = np.zeros(shape = (self.n_classes, input.shape[0]-1))
        log_beta = np.zeros(shape = (self.n_classes, input.shape[0]-1))

        for y_prime_K_minus_1 in range(self.n_classes):
            z_i_array = np.zeros(shape = (self.n_classes))
            for y_prime_K in range(self.n_classes):
                # beta[y_prime_K_minus_1, -1] += math.exp(target_unary_log_factors[y_prime_K , -1] + self.lateral_weights[y_prime_K_minus_1, y_prime_K])
                z_i_array[y_prime_K] = target_unary_log_factors[y_prime_K , -1] + self.lateral_weights[y_prime_K_minus_1, y_prime_K]
            max_z_i = np.max(z_i_array)
            log_beta[y_prime_K_minus_1, -1] = math.log(np.sum(np.exp(z_i_array - max_z_i))) + max_z_i

        for k in range(input.shape[0]-3, -1, -1):

            for y_prime_k_minus1 in range(self.n_classes):
                z_i_array = np.zeros(shape = (self.n_classes))
                for y_prime_k in range(self.n_classes):
                    # beta[y_prime_k_minus1, k] += math.exp(target_unary_log_factors[y_prime_k, k+1] + self.lateral_weights[y_prime_K_minus_1, y_prime_k ])\
                    #                                   * beta[y_prime_k,k + 1]
                    z_i_array[y_prime_k] = target_unary_log_factors[y_prime_k, k+1] + self.lateral_weights[y_prime_k_minus1, y_prime_k] \
                                           + log_beta[y_prime_k, k + 1]
                max_z_i = np.max(z_i_array)
                log_beta[y_prime_k_minus1, k] = math.log(np.sum(np.exp(z_i_array - max_z_i))) + max_z_i

        # Z_of_X = 0
        z_i_array = np.zeros(shape = (self.n_classes))
        for y_prime_1 in range(self.n_classes):
            # Z_of_X += math.exp(target_unary_log_factors[y_prime_1, 0]) * beta[y_prime_1, 0]
            z_i_array[y_prime_1] = target_unary_log_factors[y_prime_1, 0] + log_beta[y_prime_1, 0]

        max_z_i = np.max(z_i_array)
        log_Z_of_X_beta = math.log(np.sum(np.exp(z_i_array - max_z_i))) + max_z_i

        # print math.log(Z_of_X)
        # print log_Z_of_X_beta

        return log_beta, log_Z_of_X_beta


    def belief_propagation(self,input, target_unary_log_factors):
        """
        Returns the alpha/beta tables (i.e. the factor messages) using
        belief propagation (which is equivalent to forward-backward in HMMs).
        """
        log_alpha, log_Z_of_X_alpha = self.calc_log_alpha_table(target_unary_log_factors, input)

        log_beta, log_Z_of_X_beta = self.calc_log_beta_table(target_unary_log_factors, input)

        assert(int(log_Z_of_X_beta) == int(log_Z_of_X_beta))

        return log_alpha, log_beta, log_Z_of_X_alpha

    def training_loss(self,target,target_unary_log_factors, log_z_x):
        """
        Computation of the loss:
        - returns the regularized negative log-likelihood loss associated with the
          given the true target, the unary log factors of the target space and alpha/beta tables
        """
        nominator = 0
        for y_k_index, y_k in enumerate(target):
            nominator += target_unary_log_factors[y_k, y_k_index]
            if y_k_index >= 1:
                nominator += self.lateral_weights[target[y_k_index-1], y_k]


        loss = -1 * nominator + log_z_x # + self.L2 * np.sum(np.square(self.weights)) + self.L1 * np.sum(np.abs(self.weights))
        return loss

    @abstractmethod
    def bprop(self, input, target, log_marginal_probs, log_pairwise_marginal_probs):

        """
        Backpropagation:
        - fills in the CRF gradients of the weights, lateral weights and bias
          in self.grad_weights, self.grad_lateral_weights and self.grad_bias
        - returns nothing
        Argument ``input`` is a Numpy 2D array where the number of
        rows if the sequence size and the number of columns is the
        input size.
        Argument ``target`` is a Numpy 1D array of integers between
        0 and nb. of classe - 1. Its size is the same as the number of
        rows of argument ``input``.
        """
        pass

    @abstractmethod
    def update(self):
        """
        Stochastic gradient update:
        - performs a gradient step update of the CRF parameters self.weights,
          self.lateral_weights and self.bias, using the gradients in
          self.grad_weights, self.grad_lateral_weights and self.grad_bias
        """
        pass

    def use(self,dataset):
        """
        Computes and returns the outputs of the Learner for
        ``dataset``:
        - the outputs should be a list of size ``len(dataset)``, containing
          a Numpy 1D array that gives the class prediction for each position
          in the sequence, for each input sequence in ``dataset``
        Argument ``dataset`` is an MLProblem object.
        """

        outputs = []

        for input, target in dataset:
            alpha_star = np.zeros(shape = (self.n_classes, input.shape[0]-1))
            alpha_direction = np.zeros(shape = (self.n_classes, input.shape[0]-1))

            output = []

            target_unary_log_factors = self.calc_target_unary_log_factors(input)
            for label in range(self.n_classes):
                alpha_star[label, 0] = np.max(np.matrix(target_unary_log_factors[:, 0]) + self.lateral_weights[:, label])
                alpha_direction[label, 0] = np.argmax(np.matrix(target_unary_log_factors[:, 0]) + self.lateral_weights[:, label])

            for sequence_index in range(1, input.shape[0]-1):
                for label in range(self.n_classes):
                    if sequence_index < len(target) - 1:
                        alpha_star[label, sequence_index] = np.max(np.matrix(target_unary_log_factors[:, sequence_index])
                                                                            + self.lateral_weights[:, label] + np.matrix(alpha_star[:, sequence_index - 1]))
                        alpha_direction[label, sequence_index] = np.argmax(np.matrix(target_unary_log_factors[:, sequence_index])
                                                                            + self.lateral_weights[:, label] + np.matrix(alpha_star[:, sequence_index - 1]))

            P_y_K = np.max(np.matrix(target_unary_log_factors[:, -1]) + np.matrix(alpha_star[:, -1]))
            y_K = np.argmax(np.matrix(target_unary_log_factors[:, -1]) + np.matrix(alpha_star[:, -1]))
            output = output + [y_K]
            for sequence_index in range(input.shape[0]-2, -1, -1):
                output = [alpha_direction[output[0], sequence_index]] + output

            outputs = outputs + [output]

        return outputs

    def test(self,dataset):
        """
        Computes and returns the outputs of the Learner as well as the errors of the
        CRF for ``dataset``:
        - the errors should be a list of size ``len(dataset)``, containing
          a pair ``(classif_errors,nll)`` for each examples in ``dataset``, where
            - ``classif_errors`` is a Numpy 1D array that gives the class prediction error
              (0/1) at each position in the sequence
            - ``nll`` is a positive float giving the regularized negative log-likelihood of the target given
              the input sequence
        Argument ``dataset`` is an MLProblem object.
        """
        errors = []
        outputs = self.use(dataset)
        sample_index = 0
        for input, target in dataset:
            loss, log_marginal_probs, log_pairwise_marginal_probs, p_y_given_x = self.fprop(input,target)

            errors = errors + [(np.array(target == outputs[sample_index]), loss)]

            sample_index += 1

        return outputs, errors

    @abstractmethod
    def verify_gradients(self):
        """
        Verifies the implementation of the fprop and bprop methods
        using a comparison with a finite difference approximation of
        the gradients.
        """
        pass
