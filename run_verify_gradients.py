import sys
print('Python %s on %s' % (sys.version, sys.platform))
sys.path.extend(['/home/hossein/libs'])

from LinearChainCRF import *
from NNLinearChainCRF import *

print "Verifying gradients without regularization"
m = NNLinearChainCRF()
m.L2 = 0
m.L1 = 0
m.verify_gradients()

print ""
print "Verifying gradients with L2 regularization"
m.L2 = 0.001
m.verify_gradients()

print ""
print "Verifying gradients with L1 regularization"
m.L2 = 0
m.L1 = 0.001
m.verify_gradients()

