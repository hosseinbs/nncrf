import sys
import math
from statsmodels.graphics.regressionplots import abline_plot

print('Python %s on %s' % (sys.version, sys.platform))
sys.path.extend(['/home/hossein/libs'])

from mlpython.learners.generic import Learner
import numpy as np
from AbstractLinearChainCRF import *
import numpy.matlib

class NNLinearChainCRF(AbstractLinearChainCRF):
    """
    Linear chain conditional random field. The contex window size
    has a radius of 1.

    Option ``lr`` is the learning rate.

    Option ``dc`` is the decrease constante for the learning rate.

    Option ``L2`` is the L2 regularization weight (weight decay).

    Option ``L1`` is the L1 regularization weight (weight decay).

    Option ``n_epochs`` number of training epochs.

    **Required metadata:**

    * ``'input_size'``: Size of the input.
    * ``'targets'``:    Set of possible targets.

    """

    def __init__(self,
                 lr=0.001,
                 dc=1e-8,
                 L2=0.001,
                 L1=0,
                 n_epochs=10,
                 hidden_layer_size=30,
                 seed=1234,
                 tanh=True): # I have only implemented NN with one hidden layer
        self.lr=lr
        self.dc=dc
        self.L2=L2
        self.L1=L1
        self.n_epochs=n_epochs
        self.hidden_layer_size = hidden_layer_size

        # internal variable keeping track of the number of training iterations since initialization
        self.epoch = 0
        self.seed = seed
        if tanh:
            self.compute_h = self.compute_tanh
            self.compute_h_gradient = self.compute_tanh_gradient
        else:
            self.compute_h = self.compute_sigmoid
            self.compute_h_gradient = self.compute_sigmoid_gradient

    def initialize(self, input_size, n_classes):
        """
        This method allocates memory for the fprop/bprop computations
        and initializes the parameters of the CRF to 0 (DONE)
        """
        # super(NNLinearChainCRF, self).initialize(input_size, n_classes)
        self.n_classes = n_classes
        self.input_size= input_size

        self.weights = np.array([np.zeros((self.hidden_layer_size,self.n_classes)),
                        np.zeros((self.hidden_layer_size,self.n_classes)),
                        np.zeros((self.hidden_layer_size,self.n_classes))])

        self.bias = np.zeros((self.n_classes))

        self.lateral_weights = np.zeros((self.n_classes,self.n_classes))

        self.n_updates = 0 # To keep track of the number of updates, to decrease the learning rate

        self.NNweights = np.array([np.zeros((self.input_size,self.hidden_layer_size)),
                        np.zeros((self.input_size,self.hidden_layer_size)),
                        np.zeros((self.input_size,self.hidden_layer_size))])

        self.NNbias = np.zeros((self.hidden_layer_size))

        #########################
        # Initialize parameters #
        #########################
        self.rng = np.random.mtrand.RandomState(self.seed)   # create random number generator

        b = (6/float(self.hidden_layer_size + self.input_size))**0.5
        self.NNweights += [b*(self.rng.rand(self.input_size,self.hidden_layer_size) - 0.5),
                           b*(self.rng.rand(self.input_size,self.hidden_layer_size) - 0.5),
                           b*(self.rng.rand(self.input_size,self.hidden_layer_size) - 0.5)]

        b= (6/float(self.hidden_layer_size + self.n_classes))**0.5
        self.weights += [b*(self.rng.rand(self.hidden_layer_size,self.n_classes) - 0.5),
                         b*(self.rng.rand(self.hidden_layer_size,self.n_classes) - 0.5),
                         b*(self.rng.rand(self.hidden_layer_size,self.n_classes) - 0.5)]

    def calc_target_unary_log_factors(self, input):

        target_unary_log_factors = np.zeros(shape=(self.n_classes, input.shape[0]))

        for x_k_index, x_k in enumerate(input):
            if x_k_index == 0:
                a_xk_minus1 = np.dot(self.compute_h(np.zeros(shape=(self.input_size)), 0), self.weights[0])
            else:
                a_xk_minus1 = np.dot(self.compute_h(input[x_k_index - 1], 0), self.weights[0])

            if x_k_index == input.shape[0] - 1:
                a_xk_plus1 = np.dot(self.compute_h(np.zeros(shape=(self.input_size)), 2), self.weights[2])
            else:
                a_xk_plus1 = np.dot(self.compute_h(input[x_k_index + 1], 2), self.weights[2])

            a_xk = np.dot(self.compute_h(x_k, 1), self.weights[1]) + self.bias

            target_unary_log_factors[:, x_k_index] = a_xk_minus1 + a_xk + a_xk_plus1

        return target_unary_log_factors

    def compute_tanh(self, input, index):
        exptwoact = np.exp(-2*(self.NNbias+np.dot(input,self.NNweights[index,:,:])))
        return (1-exptwoact)/(exptwoact+1)

    def compute_sigmoid(self, input, index):
        act = self.NNbias + np.dot(input,self.NNweights[index,:,:])
        return 1./(1+np.exp(-act))

    def compute_tanh_gradient(self, input, index):
        exptwoact = np.exp(-2*(self.NNbias+np.dot(input,self.NNweights[index,:,:])))
        g_a = (1-exptwoact)/(exptwoact+1)
        return 1 - np.multiply(g_a, g_a)

    def compute_sigmoid_gradient(self, input, index):
        act = self.NNbias + np.dot(input,self.NNweights[index,:,:])
        g_a = 1./(1+np.exp(act))
        return np.multiply(g_a,1- g_a)

    def bprop(self, input, target, log_marginal_probs, log_pairwise_marginal_probs):

        """
        Backpropagation:
        - fills in the CRF gradients of the weights, lateral weights and bias
          in self.grad_weights, self.grad_lateral_weights and self.grad_bias
        - returns nothing
        Argument ``input`` is a Numpy 2D array where the number of
        rows if the sequence size and the number of columns is the
        input size.
        Argument ``target`` is a Numpy 1D array of integers between
        0 and nb. of classe - 1. Its size is the same as the number of
        rows of argument ``input``.
        """
        self.grad_weights = np.zeros(shape=self.weights.shape)
        self.grad_bias = np.zeros(shape=self.bias.shape)
        self.grad_lateral_weights = np.zeros(shape=self.lateral_weights.shape)
        self.grad_NNweights = np.zeros(shape=self.NNweights.shape)
        self.grad_NNbias = np.matlib.zeros(shape=self.NNbias.shape)

        for k in range(len(target)):

            e_k = np.zeros((self.n_classes))
            e_kplus1 = np.zeros((self.n_classes))

            e_k[target[k]] = 1
            grad_bias_update = -1 * (e_k - np.exp(log_marginal_probs[:,k]))

            self.grad_bias += grad_bias_update

            grad_a1_xk = np.multiply(np.dot(np.matrix(self.weights[1,:,:]), grad_bias_update), self.compute_h_gradient(input[k, :], 1))
            grad_a1_xk_minus1 = np.multiply(np.dot(np.matrix(self.weights[0,:,:]), grad_bias_update), self.compute_h_gradient(np.zeros(self.input_size), 0))
            grad_a1_xk_plus1 = np.multiply(np.dot(np.matrix(self.weights[2,:,:]), grad_bias_update), self.compute_h_gradient(np.zeros(self.input_size), 2))


            input_xk_minus1 = np.zeros(self.input_size)
            input_xk_plus1 = np.zeros(self.input_size)
            input_xk = input[k,:]

            if k != 0:
                input_xk_minus1 = input[k-1,:]
                grad_a1_xk_minus1 = np.multiply(np.dot(np.matrix(self.weights[0,:,:]), grad_bias_update), self.compute_h_gradient(input[k-1, :], 0))

            if k != (len(target) - 1):
                input_xk_plus1 = input[k+1,:]
                grad_a1_xk_plus1 = np.multiply(np.dot(np.matrix(self.weights[2,:,:]), grad_bias_update), self.compute_h_gradient(input[k+1, :], 2))
                e_kplus1[target[k+1]] = 1
                self.grad_lateral_weights += -1 * (np.dot(np.matrix(e_k).T, np.matrix(e_kplus1)) - np.exp(log_pairwise_marginal_probs[:,:,k]))

            self.grad_weights += [np.dot(np.matrix(self.compute_h(input_xk_minus1, 0)).T, np.matrix(grad_bias_update)),
                                np.dot(np.matrix(self.compute_h(input_xk, 1)).T, np.matrix(grad_bias_update)),
                                np.dot(np.matrix(self.compute_h(input_xk_plus1, 2)).T, np.matrix(grad_bias_update))]

            self.grad_NNweights += [np.dot(np.matrix(input_xk_minus1).T, grad_a1_xk_minus1),
                                   np.dot(np.matrix(input_xk).T, grad_a1_xk),
                                   np.dot(np.matrix(input_xk_plus1).T, grad_a1_xk_plus1)]

            self.grad_NNbias += np.matrix(grad_a1_xk_minus1 + grad_a1_xk + grad_a1_xk_plus1)

        self.grad_weights += 2 * self.L2 * np.array(self.weights)
        self.grad_NNweights += 2 * self.L2 * np.array(self.NNweights)


    def update(self):
        """
        Stochastic gradient update:
        - performs a gradient step update of the CRF parameters self.weights,
          self.lateral_weights and self.bias, using the gradients in
          self.grad_weights, self.grad_lateral_weights and self.grad_bias
        """
        self.weights = self.weights - self.lr * self.grad_weights
        self.lateral_weights = self.lateral_weights - self.lr * self.grad_lateral_weights
        self.bias = self.bias - self.lr * self.grad_bias
        self.NNweights = self.NNweights - self.lr * self.grad_NNweights
        self.NNbias = self.NNbias - self.lr * self.grad_NNbias
