import sys
import math
from statsmodels.graphics.regressionplots import abline_plot

print('Python %s on %s' % (sys.version, sys.platform))
sys.path.extend(['/home/hossein/libs'])

from mlpython.learners.generic import Learner
import numpy as np
from AbstractLinearChainCRF import *
import numpy.matlib

class NNLinearChainCRF(AbstractLinearChainCRF):
    """
    Linear chain conditional random field. The contex window size
    has a radius of 1.

    Option ``lr`` is the learning rate.

    Option ``dc`` is the decrease constante for the learning rate.

    Option ``L2`` is the L2 regularization weight (weight decay).

    Option ``L1`` is the L1 regularization weight (weight decay).

    Option ``n_epochs`` number of training epochs.

    **Required metadata:**

    * ``'input_size'``: Size of the input.
    * ``'targets'``:    Set of possible targets.

    """

    def __init__(self,
                 lr=0.001,
                 dc=1e-8,
                 L2=0.001,
                 L1=0,
                 n_epochs=10,
                 hidden_layer_size=30,
                 seed=1234,
                 tanh=True,
                 context_window_size = -2): # I have only implemented NN with one hidden layer
        self.lr=lr
        self.dc=dc
        self.L2=L2
        self.L1=L1
        self.n_epochs=n_epochs
        self.hidden_layer_size = hidden_layer_size

        # internal variable keeping track of the number of training iterations since initialization
        self.epoch = 0
        self.seed = seed
        if tanh:
            self.compute_h = self.compute_tanh
            self.compute_h_gradient = self.compute_tanh_gradient
        else:
            self.compute_h = self.compute_sigmoid
            self.compute_h_gradient = self.compute_sigmoid_gradient

        if(context_window_size == 0):
            self.context_window_indices = [0]
        elif(context_window_size < -1):
            self.context_window_indices = np.arange(context_window_size, 1)
        else:
            self.context_window_indices = np.arange(-context_window_size, context_window_size + 1)

    def initialize(self, input_size, n_classes):
        """
        This method allocates memory for the fprop/bprop computations
        and initializes the parameters of the CRF to 0 (DONE)
        """
        # super(NNLinearChainCRF, self).initialize(input_size, n_classes)
        self.n_classes = n_classes
        self.input_size= input_size

        self.weights = np.zeros((len(self.context_window_indices), self.hidden_layer_size, self.n_classes))

        self.bias = np.zeros((self.n_classes))

        self.lateral_weights = np.zeros((self.n_classes,self.n_classes))

        self.n_updates = 0 # To keep track of the number of updates, to decrease the learning rate

        # = np.zeros((len(self.context_window_indices), self.input_size, self.n_classes))

        self.NNweights = np.zeros((len(self.context_window_indices), self.input_size, self.hidden_layer_size))

        self.NNbias = np.zeros((self.hidden_layer_size))

        #########################
        # Initialize parameters #
        #########################
        self.rng = np.random.mtrand.RandomState(self.seed)   # create random number generator

        b1 = (6/float(self.hidden_layer_size + self.input_size))**0.5
        b2 = (6/float(self.hidden_layer_size + self.n_classes))**0.5
        for weight_ind in range(len(self.context_window_indices)):
            self.NNweights[weight_ind, :, :] += np.array(b1*(self.rng.rand(self.input_size,self.hidden_layer_size) - 0.5))
            self.weights[weight_ind, :, :] += np.array(b2*(self.rng.rand(self.hidden_layer_size,self.n_classes) - 0.5))


    def calc_target_unary_log_factors(self, input):

        target_unary_log_factors = np.zeros(shape=(self.n_classes, input.shape[0]))

        for x_k_index, x_k in enumerate(input):

            a_xk = 0
            for weight_index, context_window_index in enumerate(self.context_window_indices):
                if (x_k_index + context_window_index) >= 0 and (x_k_index + context_window_index) < input.shape[0]:

                    a_xk += np.dot(self.compute_h(input[x_k_index + context_window_index], weight_index), self.weights[weight_index,:,:])

            a_xk += self.bias
            # if x_k_index == 0:
            #     a_xk_minus1 = np.dot(self.compute_h(np.zeros(shape=(self.input_size)), 0), self.weights[0])
            # else:
            #     a_xk_minus1 = np.dot(self.compute_h(input[x_k_index - 1], 0), self.weights[0])
            #
            # if x_k_index == input.shape[0] - 1:
            #     a_xk_plus1 = np.dot(self.compute_h(np.zeros(shape=(self.input_size)), 2), self.weights[2])
            # else:
            #     a_xk_plus1 = np.dot(self.compute_h(input[x_k_index + 1], 2), self.weights[2])
            #
            # a_xk = np.dot(self.compute_h(x_k, 1), self.weights[1]) + self.bias
            target_unary_log_factors[:, x_k_index] = a_xk

        return target_unary_log_factors

    def compute_tanh(self, input, index):
        exptwoact = np.exp(-2*(self.NNbias+np.dot(input,self.NNweights[index,:,:])))
        return (1-exptwoact)/(exptwoact+1)

    def compute_sigmoid(self, input, index):
        act = self.NNbias + np.dot(input,self.NNweights[index,:,:])
        return 1./(1+np.exp(-act))

    def compute_tanh_gradient(self, input, index):
        exptwoact = np.exp(-2*(self.NNbias+np.dot(input,self.NNweights[index,:,:])))
        g_a = (1-exptwoact)/(exptwoact+1)
        return 1 - np.multiply(g_a, g_a)

    def compute_sigmoid_gradient(self, input, index):
        act = self.NNbias + np.dot(input,self.NNweights[index,:,:])
        g_a = 1./(1+np.exp(act))
        return np.multiply(g_a,1- g_a)

    def bprop(self, input, target, log_marginal_probs, log_pairwise_marginal_probs):

        """
        Backpropagation:
        - fills in the CRF gradients of the weights, lateral weights and bias
          in self.grad_weights, self.grad_lateral_weights and self.grad_bias
        - returns nothing
        Argument ``input`` is a Numpy 2D array where the number of
        rows if the sequence size and the number of columns is the
        input size.
        Argument ``target`` is a Numpy 1D array of integers between
        0 and nb. of classe - 1. Its size is the same as the number of
        rows of argument ``input``.
        """
        self.grad_weights = np.zeros(shape=self.weights.shape)
        self.grad_bias = np.zeros(shape=self.bias.shape)
        self.grad_lateral_weights = np.zeros(shape=self.lateral_weights.shape)
        self.grad_NNweights = np.zeros(shape=self.NNweights.shape)
        self.grad_NNbias = np.matlib.zeros(shape=self.NNbias.shape)

        for k in range(len(target)):

            e_k = np.zeros((self.n_classes))
            e_kplus1 = np.zeros((self.n_classes))

            e_k[target[k]] = 1
            grad_bias_update = -1 * (e_k - np.exp(log_marginal_probs[:,k]))

            for weight_index, context_window_index in enumerate(self.context_window_indices):
                if (k + context_window_index) >= 0 and (k + context_window_index) < input.shape[0]:
                    grad = np.multiply(np.dot(np.matrix(self.weights[weight_index,:,:]), grad_bias_update),
                                    self.compute_h_gradient(input[k + context_window_index, :], weight_index))
                    self.grad_weights[weight_index,:,:] += \
                        np.dot(np.matrix(self.compute_h(input[k + context_window_index,:], weight_index)).T, np.matrix(grad_bias_update))

                    self.grad_NNweights[weight_index,:,:] += np.dot(np.matrix(input[k + context_window_index,:]).T, grad)

                    self.grad_NNbias += grad

            self.grad_bias += grad_bias_update

            if k < (len(target) - 1):
                e_kplus1[target[k+1]] = 1
                self.grad_lateral_weights += -1 * (np.dot(np.matrix(e_k).T, np.matrix(e_kplus1)) - np.exp(log_pairwise_marginal_probs[:,:,k]))

        self.grad_weights += 2 * self.L2 * np.array(self.weights)
        self.grad_NNweights += 2 * self.L2 * np.array(self.NNweights)


    def update(self):
        """
        Stochastic gradient update:
        - performs a gradient step update of the CRF parameters self.weights,
          self.lateral_weights and self.bias, using the gradients in
          self.grad_weights, self.grad_lateral_weights and self.grad_bias
        """
        self.weights = self.weights - self.lr * self.grad_weights
        self.lateral_weights = self.lateral_weights - self.lr * self.grad_lateral_weights
        self.bias = self.bias - self.lr * self.grad_bias
        self.NNweights = self.NNweights - self.lr * self.grad_NNweights
        self.NNbias = self.NNbias - self.lr * self.grad_NNbias

    def verify_gradients(self):
        """
        Verifies the implementation of the fprop and bprop methods
        using a comparison with a finite difference approximation of
        the gradients.
        """
        print 'WARNING: calling verify_gradients reinitializes the learner'

        rng = np.random.mtrand.RandomState(1234)

        self.initialize(10,3)
        example = (rng.rand(4,10),np.array([0,1,1,2]))
        input,target = example
        epsilon=1e-6
        self.lr = 0.1
        self.decrease_constant = 0

        self.NNbias = 0.01*rng.rand(self.hidden_layer_size)
        self.bias = 0.01*rng.rand(self.n_classes)

        self.lateral_weights = 0.01*rng.rand(self.n_classes,self.n_classes)

        loss, log_marginal_probs, log_pairwise_marginal_probs,p_y_given_x = self.fprop(input, target)
        self.bprop(input,target, log_marginal_probs, log_pairwise_marginal_probs)

        import copy
        emp_grad_weights = copy.deepcopy(self.weights)

        for h in range(len(self.weights)):
            for i in range(self.weights[h].shape[0]):
                for j in range(self.weights[h].shape[1]):
                    self.weights[h][i,j] += epsilon

                    loss_a, log_marginal_probs_a, log_pairwise_marginal_probs_a, p_y_given_x_a = self.fprop(input, target)
                    self.weights[h][i,j] -= epsilon

                    self.weights[h][i,j] -= epsilon
                    loss_b, log_marginal_probs_b, log_pairwise_marginal_probs_b, p_y_given_x_b = self.fprop(input, target)
                    self.weights[h][i,j] += epsilon

                    emp_grad_weights[h][i,j] = (loss_a-loss_b)/(2.*epsilon)

        for weight_ind, context_window_index  in enumerate(self.context_window_indices):
            print('grad_weights['+ str(context_window_index) +'] diff.:' +  str(np.sum(np.abs(self.grad_weights[weight_ind,:,:].ravel()-emp_grad_weights[weight_ind,:,:].ravel()))/self.weights[weight_ind,:,:].ravel().shape[0]))

        emp_grad_NNweights = copy.deepcopy(self.NNweights)
        for h in range(len(self.NNweights)):
            for i in range(self.NNweights[h].shape[0]):
                for j in range(self.NNweights[h].shape[1]):
                    self.NNweights[h][i,j] += epsilon

                    loss_a, log_marginal_probs_a, log_pairwise_marginal_probs_a, p_y_given_x_a = self.fprop(input, target)
                    self.NNweights[h][i,j] -= epsilon

                    self.NNweights[h][i,j] -= epsilon
                    loss_b, log_marginal_probs_b, log_pairwise_marginal_probs_b, p_y_given_x_b = self.fprop(input, target)
                    self.NNweights[h][i,j] += epsilon

                    emp_grad_NNweights[h][i,j] = (loss_a-loss_b)/(2.*epsilon)

        for weight_ind, context_window_index  in enumerate(self.context_window_indices):
            print('grad_NNweights['+ str(context_window_index) +'] diff.:' +  str(np.sum(np.abs(self.grad_NNweights[weight_ind,:,:].ravel()-emp_grad_NNweights[weight_ind,:,:].ravel()))/self.NNweights[weight_ind,:,:].ravel().shape[0]))

        emp_grad_lateral_weights = copy.deepcopy(self.lateral_weights)

        for i in range(self.lateral_weights.shape[0]):
            for j in range(self.lateral_weights.shape[1]):
                self.lateral_weights[i,j] += epsilon
                loss_a, log_marginal_probs_a, log_pairwise_marginal_probs_a, p_y_given_x_a = self.fprop(input, target)
                self.lateral_weights[i,j] -= epsilon

                self.lateral_weights[i,j] -= epsilon
                loss_b, log_marginal_probs_b, log_pairwise_marginal_probs_b, p_y_given_x_b = self.fprop(input, target)
                self.lateral_weights[i,j] += epsilon

                emp_grad_lateral_weights[i,j] = (loss_a-loss_b)/(2.*epsilon)


        print 'grad_lateral_weights diff.:',np.sum(np.abs(self.grad_lateral_weights.ravel()-emp_grad_lateral_weights.ravel()))/self.lateral_weights.ravel().shape[0]

        emp_grad_bias = copy.deepcopy(self.bias)
        for i in range(self.bias.shape[0]):
            self.bias[i] += epsilon
            loss_a, log_marginal_probs_a, log_pairwise_marginal_probs_a, p_y_given_x_a = self.fprop(input, target)
            self.bias[i] -= epsilon

            self.bias[i] -= epsilon
            loss_b, log_marginal_probs_b, log_pairwise_marginal_probs_b, p_y_given_x_b = self.fprop(input, target)
            self.bias[i] += epsilon

            emp_grad_bias[i] = (loss_a-loss_b)/(2.*epsilon)

        print 'grad_bias diff.:',np.sum(np.abs(self.grad_bias.ravel()-emp_grad_bias.ravel()))/self.bias.ravel().shape[0]

        emp_grad_NNbias = copy.deepcopy(self.NNbias)
        for i in range(self.NNbias.shape[0]):
            self.NNbias[i] += epsilon
            loss_a, log_marginal_probs_a, log_pairwise_marginal_probs_a, p_y_given_x_a = self.fprop(input, target)
            self.NNbias[i] -= epsilon

            self.NNbias[i] -= epsilon
            loss_b, log_marginal_probs_b, log_pairwise_marginal_probs_b, p_y_given_x_b = self.fprop(input, target)
            self.NNbias[i] += epsilon

            emp_grad_NNbias[i] = (loss_a-loss_b)/(2.*epsilon)

        print 'grad_NNbias diff.:',np.sum(np.abs(self.grad_NNbias.ravel()-emp_grad_NNbias.ravel()))/self.NNbias.ravel().shape[0]
