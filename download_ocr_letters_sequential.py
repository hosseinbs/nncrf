import sys
print('Python %s on %s' % (sys.version, sys.platform))
sys.path.extend(['/home/hossein/libs'])
sys.path.extend(['/home/hossein/libs/ml_datasets'])
print sys.path



import ocr_letters_sequential as mldataset
import os


# repo = os.environ.get('MLPYTHON_DATASET_REPO')
# if repo is None:
#     raise ValueError('environment variable MLPYTHON_DATASET_REPO is not defined')
dataset_dir = '/home/hossein/libs/ml_datasets/ocr_letters_sequential'

if not os.path.exists(dataset_dir):
    os.makedirs(dataset_dir)
mldataset.obtain(dataset_dir)

